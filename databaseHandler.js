const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

function storeElement(zip,shop,location){

    dynamo.put({
        TableName: process.env.dynamoDBZipShopTableName,
        Item: {
          zip: zip,
          shop: shop,
          location: location
        }
    });

}


function deleteElement(zip,shop){

    dynamo.delete({
        TableName: process.env.dynamoDBZipShopTableName,
        Key: {
            zip: zip,
            shop: shop
        }
      })

}

