const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();
var csv = require('fast-csv');

module.exports.createZipFromCSV = (event, context, callback) => {

    var postZips = []
    csv.fromPath('plz.csv')
        .on('data', function(data){
            postZips.push(data[0])
        })
        .on('end', function(){
            console.log('done loading PLZs');

            for (var i = 0, len = postZips.length; i < len; i++) {

                var params = {
                    TableName: process.env.dynamoDBZipTableName,
                    Item:{
                        "zip": postZips[i]
                    }
                };

                dynamo.put(params, function(err, data) {
                    if (err) {
                        console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
                    } else {
                        console.log("Added item:", JSON.stringify(data, null, 2));
                    }
                });
            }
        });  

}
